import * as L from 'leaflet';
import { General } from 'src/models/General';

export function gerarMapa(g: General) {
  try {
    for (const a of g.mapa_arr) {
      const marker = L.marker([a.Latitude, a.Longitude]);
      var mensagem: any;
      marker.on('mouseover', (e) => {
        mensagem = L.popup({ offset: L.point(0, -20) });
        mensagem.setContent(
          `<strong>${a.Nome}</strong>` +
            `<br><strong>Nº Colmeias:</strong> ${
              g.colm_arr.filter((x) => x.Apiario == a.Apiario).length
            }`
        );
        mensagem.setLatLng(e.target.getLatLng());
        mensagem.openOn(g.mapa_map);
      });

      marker.on('mouseout', (e) => {
        g.mapa_map.closePopup(mensagem);
      });

      marker.addTo(g.mapa_map);
    }
  } catch (error) {
    console.error('Mapa Indisponível');
  }
  return g;
}
