import axios, { AxiosResponse } from 'axios';
import { ResponseWS } from 'src/models/WS_Model';

export const getDados = async () =>
  (
    await axios.get<AxiosResponse<ResponseWS>, AxiosResponse<ResponseWS>>(
      'http://100.77.4.191:3000/dados'
    )
  ).data;

export const postCadastro = async (method: string, body: string) =>
  (
    await axios.post<AxiosResponse<ResponseWS>, AxiosResponse<ResponseWS>>(
      `http://100.77.4.191:3000/${method}`,
      body,
      { headers: { 'Content-Type': 'application/json' } }
    )
  ).data;
