import { MenuItem } from 'primeng/api';

const Menus: MenuItem[] = [
  {
    label: 'Inicio',
    command: () => showPanel(1),
  },
  {
    label: 'Cadastros',
    items: [
      {
        label: 'Produtores',
        command: () => showPanel(2),
      },
      {
        label: 'Ténicos',
        command: () => showPanel(3),
      },
      {
        label: 'Apiarios',
        command: () => showPanel(4),
      },
      {
        label: 'Colmeias',
        command: () => showPanel(5),
      },
    ],
  },
];

type panelType = { [k: string]: boolean };
const Panels: panelType = {
  hideInicio: false,
  hideProdutores: true,
  hideTecnicos: true,
  hideApiculturas: true,
  hideColmeias: true,
};

function showMenus(
  inicial: number,
  panels: number[],
  disabled: boolean
): MenuItem {
  for (const i of panels) {
    Menus[i - 1].visible = true;
    if (i !== 1) Menus[i - 1].disabled = disabled;
  }
  return showPanel(inicial);
}

function showPanel(panel: number): MenuItem {
  Object.keys(Panels).forEach((k) => (Panels[k] = true));
  if (panel === 1) Panels['hideInicio'] = false;
  if (panel === 2) Panels['hideProdutores'] = false;
  if (panel === 3) Panels['hideTecnicos'] = false;
  if (panel === 4) Panels['hideApiculturas'] = false;
  if (panel === 5) Panels['hideColmeias'] = false;
  return Menus[panel - 1];
}

export { Panels, Menus, showMenus };
