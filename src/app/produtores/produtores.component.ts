import { Component, Input, OnInit } from '@angular/core';
import { General } from 'src/models/General';

@Component({
  selector: 'app-produtores',
  templateUrl: './produtores.component.html',
  styleUrls: ['./produtores.component.scss'],
})
export class ProdutoresComponent implements OnInit {
  @Input() g!: General;

  constructor() {}

  public ngOnInit(): void {}
}
