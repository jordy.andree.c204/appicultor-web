import { Component, Input, OnInit } from '@angular/core';
import { General } from 'src/models/General';

@Component({
  selector: 'app-tecnicos',
  templateUrl: './tecnicos.component.html',
  styleUrls: ['./tecnicos.component.scss'],
})
export class TecnicosComponent implements OnInit {
  @Input() g!: General;

  constructor() {}

  public ngOnInit(): void {}
}
