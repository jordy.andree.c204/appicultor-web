import { Component, Input, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { gerarMapa } from 'src/functions/General_Functions';
import { General } from 'src/models/General';

export const DEFAULT_LAT = -14.2401;
export const DEFAULT_LON = -53.1805;
export var TITULO = 'Proyecto';
const iconRetinaUrl = 'assets/img/honey-dipper.png';
const iconUrl = 'assets/img/honey-dipper.png';
const shadowUrl = 'assets/img/honey-dipper.png';

const southWest = L.latLng(-38, -106);
const northEast = L.latLng(8, 8);
const bounds = L.latLngBounds(southWest, northEast);

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  @Input() g!: General;

  constructor() {}

  public async ngOnInit() {
    this.g.mapa_map = L.map('map', {
      center: [-24.980466, -53.34029],
      attributionControl: false,
      maxBounds: bounds,
      zoom: 17,
    });

    var iconDefault = L.icon({
      iconRetinaUrl,
      iconUrl,
      shadowUrl,
      iconSize: [35, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41],
    });
    L.Marker.prototype.options.icon = iconDefault;

    const tiles = L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        maxZoom: 19,
        minZoom: 15,
        attribution:
          '&copy; <a href="https://1938.com.es">Web Inteligencia Artificial</a>',
      }
    );

    tiles.addTo(this.g.mapa_map);
  }
}
