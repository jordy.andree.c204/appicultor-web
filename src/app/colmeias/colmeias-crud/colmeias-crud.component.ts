import { Component, Input, OnInit } from '@angular/core';
import { General } from 'src/models/General';

@Component({
  selector: 'app-colmeias-crud',
  templateUrl: './colmeias-crud.component.html',
  styleUrls: ['./colmeias-crud.component.scss'],
})
export class ColmeiasCrudComponent implements OnInit {
  @Input() g!: General;

  public spro: boolean = false;
  public sapi: boolean = false;

  constructor() {}

  public ngOnInit(): void {}

  public inppro() {
    this.spro = true;
  }
  public selpro() {
    this.g.colm_crud_produtor = this.g.colm_crud_produtor_obj?.Cadastro;
    this.getCol();
    this.spro = false;
  }

  public inpapi() {
    this.sapi = true;
  }
  public selapi() {
    this.g.colm_crud_apiario = this.g.colm_crud_apiario_obj?.apiario;
    this.getCol();
    this.sapi = false;
  }

  public getCol() {
    const lc = this.g.colm_arr.filter(
      (x) =>
        x.Cadastro == this.g.colm_crud_produtor &&
        x.Apiario == this.g.colm_crud_apiario
    );
    this.g.colm_crud_colmeia =
      lc.length == 0 ? 1 : lc[lc.length - 1].Colmeia + 1;
  }
}
