import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColmeiasCrudComponent } from './colmeias-crud.component';

describe('ColmeiasCrudComponent', () => {
  let component: ColmeiasCrudComponent;
  let fixture: ComponentFixture<ColmeiasCrudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColmeiasCrudComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColmeiasCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
