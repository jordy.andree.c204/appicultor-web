import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColmeiasComponent } from './colmeias.component';

describe('ColmeiasComponent', () => {
  let component: ColmeiasComponent;
  let fixture: ComponentFixture<ColmeiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColmeiasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColmeiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
