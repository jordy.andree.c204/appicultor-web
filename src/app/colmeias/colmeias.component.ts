import { Component, Input, OnInit } from '@angular/core';
import { postCadastro } from 'src/functions/WS_Axios';
import { General } from 'src/models/General';
import { Colmeias } from 'src/models/WS_Model';
import { AppService } from '../app.service';

@Component({
  selector: 'app-colmeias',
  templateUrl: './colmeias.component.html',
  styleUrls: ['./colmeias.component.scss'],
})
export class ColmeiasComponent implements OnInit {
  @Input() g!: General;

  public scad: boolean = false;

  constructor(private ap: AppService) {}

  public ngOnInit(): void {}

  public trocarShow = (t: boolean) => (this.g.colm_crud_cad = t);

  public async salvar() {
    this.g.overlay = true;
    const d = await postCadastro(
      'addColmeia',
      JSON.stringify({
        colmeia: {
          Cadastro: this.g.colm_crud_produtor,
          Apiario: this.g.colm_crud_apiario,
          Colmeia: this.g.colm_crud_colmeia,
          dataIntalacao: this.g.colm_crud_data,
        },
      })
    );
    this.g = this.ap.listaDados(this.g, d);
    this.cancelar();
    this.g.overlay = false;
  }

  public cancelar() {
    this.g.colm_crud_produtor = undefined;
    this.g.colm_crud_produtor_obj = undefined;
    this.g.colm_crud_apiario = undefined;
    this.g.colm_crud_apiario_obj = undefined;
    this.g.colm_crud_colmeia = 1;
    this.g.colm_crud_data = '';
    this.g.colm_crud_cad = false;
  }

  public detalhe(col: Colmeias) {
    /*const sapiario = this.g.apia_arr.find(
      (x) => x.apiario == col.Apiario && x.Cadastro == col.Cadastro
    )!.seqApiario;*/
    //console.dir(sapiario)

    console.dir(`${col.Apiario} - ${col.Colmeia}`)
    this.g.colm_deta_cade = this.g.cade_arr.filter(
      (x) => x.seqApiario == col.Apiario && x.SeqColmeia == col.Colmeia
    );
    console.dir(this.g.colm_deta_cade)
    this.scad = true;
  }
}
