import { Component, Input, OnInit } from '@angular/core';
import { postCadastro } from 'src/functions/WS_Axios';
import { General } from 'src/models/General';
import { AppService } from '../app.service';

@Component({
  selector: 'app-apiarios',
  templateUrl: './apiarios.component.html',
  styleUrls: ['./apiarios.component.scss'],
})
export class ApiariosComponent implements OnInit {
  @Input() g!: General;

  public scad: boolean = false;

  constructor(private ap: AppService) {}

  public ngOnInit(): void {}

  public trocarShow = (t: boolean) => (this.g.apia_crud_cad = t);

  public async salvar() {
    this.g.overlay = true;
    const d = await postCadastro(
      'addApiario',
      JSON.stringify({
        apiario: {
          Cadastro: this.g.apia_crud_produtor,
          apiario: this.g.apia_crud_apiario,
          Nome: this.g.apia_crud_nome,
          Endereco: this.g.apia_crud_endereco,
          Latitude: this.g.apia_crud_lat,
          Longitude: this.g.apia_crud_lon,
        },
      })
    );
    this.g = this.ap.listaDados(this.g, d);
    this.cancelar();
    this.g.overlay = false;
  }

  public cancelar() {
    this.g.apia_crud_produtor = undefined;
    this.g.apia_crud_produtor_obj = undefined;
    this.g.apia_crud_apiario = undefined;
    this.g.apia_crud_apiario_obj = undefined;
    this.g.apia_crud_nome = '';
    this.g.apia_crud_endereco = '';
    this.g.apia_crud_lat = undefined;
    this.g.apia_crud_lon = undefined;
    this.g.apia_crud_cad = false;
  }
}
