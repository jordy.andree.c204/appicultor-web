import { Component, Input, OnInit } from '@angular/core';
import { General } from 'src/models/General';

@Component({
  selector: 'app-apiarios-crud',
  templateUrl: './apiarios-crud.component.html',
  styleUrls: ['./apiarios-crud.component.scss'],
})
export class ApiariosCrudComponent implements OnInit {
  @Input() g!: General;

  public spro: boolean = false;
  public sapi: boolean = false;

  constructor() {}

  public ngOnInit(): void {}

  public inppro() {
    this.spro = true;
  }
  public selpro() {
    this.g.apia_crud_produtor = this.g.apia_crud_produtor_obj?.Cadastro;
    this.spro = false;
  }

  public inpapi() {
    this.sapi = true;
  }
  public selapi() {
    this.g.apia_crud_apiario = this.g.apia_crud_apiario_obj?.Apiario;
    this.g.apia_crud_nome = this.g.apia_crud_apiario_obj?.Nome!;
    this.g.apia_crud_lat = this.g.apia_crud_apiario_obj?.Latitude;
    this.g.apia_crud_lon = this.g.apia_crud_apiario_obj?.Longitude;
    this.sapi = false;
  }
}
