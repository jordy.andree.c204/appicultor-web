import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiariosComponent } from './apiarios.component';

describe('ApiariosComponent', () => {
  let component: ApiariosComponent;
  let fixture: ComponentFixture<ApiariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApiariosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApiariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
