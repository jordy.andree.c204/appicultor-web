import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import axios from 'axios';
import { MenuItem, MessageService, PrimeNGConfig } from 'primeng/api';
import * as fd from 'src/functions/Form_Design';
import { gerarMapa } from 'src/functions/General_Functions';
import { getDados } from 'src/functions/WS_Axios';
import { General } from 'src/models/General';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MessageService],
})
export class AppComponent {
  public title = 'hackathon-srd2023';

  public menus: MenuItem[] = fd.Menus;
  public activeMenu: MenuItem = {};
  public panel = fd.Panels;

  public g: General = new General();

  constructor(
    private messageService: MessageService,
    public translate: TranslateService,
    public primeNGConfig: PrimeNGConfig,
    private ap: AppService
  ) {
    translate.addLangs(['pt']);
    translate.setDefaultLang('pt');
    translate.use('pt');
    this.translate
      .stream('primeng')
      .subscribe((data) => this.primeNGConfig.setTranslation(data));
  }

  public async ngOnInit(): Promise<void> {
    axios.interceptors.request.use(
      (config) => {
        this.g.buscandoWS = true;
        return config;
      },
      (error) => {
        this.g.buscandoWS = false;
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      (response) => {
        this.g.buscandoWS = false;
        return response;
      },
      (error) => {
        if (error.response.data) {
          const e = error.response.data;
          if (e.errorCode && e.errorCode != 'entity_already_exists')
            this.messageService.add({
              severity: 'error',
              summary: 'Web service error',
              detail: e.errorMessage,
              life: 5000,
            });
          else
            this.messageService.add({
              severity: 'error',
              summary: 'Web service error',
              detail: e.msgRet ?? e.message ?? error.message,
              life: 5000,
            });
        }
        this.g.buscandoWS = false;
        return Promise.reject(error);
      }
    );

    const d = await getDados();

    this.g = this.ap.listaDados(this.g, d);
    this.g = gerarMapa(this.g);
  }
}
