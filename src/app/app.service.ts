import { Injectable } from '@angular/core';
import { getDados } from 'src/functions/WS_Axios';
import { General } from 'src/models/General';
import { ResponseWS } from 'src/models/WS_Model';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor() {}

  public listaDados(g: General, d: ResponseWS) {
    g.prod_arr = d.Produtores;
    g.tecn_arr = d.Tecnicos;

    g.mapa_arr = d.Apiarios;
    const pa = [];
    for (const i of d.ProdutoresXApiarios) {
      const p = g.prod_arr.find((x) => x.Cadastro == i.Cadastro);
      i.ProNom = p?.Cadastro + ' - ' + p?.Nome;
      pa.push(i);
    }
    g.apia_arr = pa;

    const c = [];
    for (const i of d.Colmeias) {
      const p = g.prod_arr.find((x) => x.Cadastro == i.Cadastro);
      i.ProNom = p?.Cadastro + ' - ' + p?.Nome;
      const a = g.apia_arr.find(
        (x) => x.apiario == i.Apiario && x.Cadastro == i.Cadastro
      );
      i.ApiNom = a?.apiario + ' - ' + a?.Nome;
      c.push(i);
    }
    g.colm_arr = c;

    const pa2 = [];
    for (const i of g.apia_arr) {
      i.QuantidadeColmeia = g.colm_arr.filter(
        (x) => x.Apiario == i.apiario && x.Cadastro == i.Cadastro
      ).length;
      pa2.push(i);
    }
    g.apia_arr = pa2;

    g.even_arr = d.Eventos;

    const cd = [];
    for (const i of d.Caderno) {
      i.descricao = g.even_arr.find(
        (x) => x.seqEvento == i.seqEvento
      )?.Descrição!;
      cd.push(i);
    }
    g.cade_arr = cd;

    return g;
  }
}
