import * as wsm from './WS_Model';

export class General {
  public overlay: boolean = false;
  public buscandoWS: boolean = false;

  public mapa_map!: L.Map;
  public mapa_arr: wsm.Apiarios[] = [];
  public prod_arr: wsm.Produtores[] = [];
  public prod_obj?: wsm.Produtores;
  public tecn_arr: wsm.Tecnicos[] = [];
  public apia_arr: wsm.ProdutoresXApiarios[] = [];
  public apia_obj?: wsm.ProdutoresXApiarios;
  public colm_arr: wsm.Colmeias[] = [];
  public colm_obj?: wsm.Colmeias;
  public cade_arr: wsm.Caderno[] = [];
  public even_arr: wsm.Eventos[] = [];

  public apia_crud_cad: boolean = false;
  public apia_crud_produtor?: number;
  public apia_crud_produtor_obj?: wsm.Produtores;
  public apia_crud_apiario?: number;
  public apia_crud_apiario_obj?: wsm.Apiarios;
  public apia_crud_nome: string = '';
  public apia_crud_endereco: string = '';
  public apia_crud_lat?: number;
  public apia_crud_lon?: number;

  public colm_crud_cad: boolean = false;
  public colm_crud_produtor?: number;
  public colm_crud_produtor_obj?: wsm.Produtores;
  public colm_crud_apiario?: number;
  public colm_crud_apiario_obj?: wsm.ProdutoresXApiarios;
  public colm_crud_colmeia: number = 1;
  public colm_crud_data: string = '';
  public colm_deta_cade: wsm.Caderno[] = [];
}
