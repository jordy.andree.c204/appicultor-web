export interface ResponseWS {
  Produtores: Produtores[];
  Apiarios: Apiarios[];
  ProdutoresXApiarios: ProdutoresXApiarios[];
  Colmeias: Colmeias[];
  Eventos: Eventos[];
  Tecnicos: Tecnicos[];
  Caderno: Caderno[];
}

export interface Produtores {
  Cadastro: number;
  Nome: string;
  Cpf: string;
  Matricula: string;
  Endereco: string;
  Cidade: string;
  UF: string;
  Telefone: string;
  email: string;
  CadPro: string;
  Curso: string;
}

export interface Tecnicos {
  seqTecnico: number;
  Nome: string;
  Telefone: string;
}

export interface Apiarios {
  Apiario: number;
  Nome: string;
  Latitude: number;
  Longitude: number;
}

export interface ProdutoresXApiarios {
  seqApiario: number;
  Cadastro: number;
  ProNom: string;
  apiario: number;
  Nome: string;
  Endereco: string;
  Latitude: number;
  Longitude: number;
  QuantidadeColmeia: number;
}

export interface Colmeias {
  seqColmeia: number;
  Cadastro: number;
  ProNom: string;
  Apiario: number;
  ApiNom: string;
  Colmeia: number;
  QRCode: string;
  dataIntalacao: string;
}

export interface Eventos {
  seqEvento: number;
  Descrição: string;
  Colmeia: string;
  Opções: string[];
}

export interface Caderno {
  seqCaderno: string;
  seqApiario: number;
  SeqColmeia: number;
  seqEvento: number;
  datEvento: string;
  obsEvento: string;
  quantidade: number;
  descricao: string;
}
